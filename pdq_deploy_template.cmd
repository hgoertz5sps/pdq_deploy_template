@echo off
setlocal

::Created by HG Ticket #84825, 84885
::Ticket#
::Edited By
::Date

:: This variable decies whether to install or just verify
set VerifyOnly=0
:: Check to see if running in VerifyOnly mode
if /I "%~1"=="VerifyOnly" set VerifyOnly=1

:: Do not include quotes around these variables, sub-routines add and remove quotes as needed.
:: Name of the software, used for creating entries in Event Logs
set software=
:: The full path to the installer
set installer=
:: Fill out previous two variables.  Run installation by hand to find values for the following variables
:: These are used for error checking
:: set targetdir=
:: To find the value of this variable in command line do
:: for /F "tokens=3*" %i in ('dir /-C "%dir%" ^|Find "bytes" ^|find /v "free"') Do set actualsize= %i
:: The difference is %%i vs %i, %%i when run from .cmd file %i when run directly from command line
:: set targetsize=
:: set executable=
:: set shortcut=
:: The "permissions" variable is usually not needed, most software runs with default permissions
:: The most common permissions required on an install dir is Modify for Users
:: "BUILTIN\Users:(OI)(CI)(M)"
:: To set permissions add "icacls "%targetdir%" /c /grant:r "Users":(OI)(CI)(M)"
:: set permissions=BUILTIN\Users:(OI)(CI)(M)


:: Create an event saying that softwaer is being installed
If %VerifyOnly%==0 eventcreate /ID 100 /L Application /T INFORMATION /D "Installing %software%" /SO "SPS_TECH"
If %VerifyOnly%==1 eventcreate /ID 100 /L Application /T INFORMATION /D "Checking integrity of %software%" /SO "SPS_TECH"
:: Append call to installer here.
:: If MSI use MSIEXEC /i %installer% /qn  Check manual for any additional arguments (such as license keys, EULA etc)
:: If EXE Read the manual for the software to get the manufacturer recommended method
:: If none is provided try /?, /q, /v/qn, /s, /silent, /quiet on EXE installer to see if it present but undocumented (often the case)
:: If %VerifyOnly%==0 



:: Check to see if executable exists
:: Error Code 10 if missing
:: call :filecheck "%executable%" 10

:: Check to see is shortcut exists
:: Error Code 11 if missing
:: call :filecheck "%shortcut%" 11

:: Check if installed content is correct size
:: Error Code 12 if Content Size is incorrect
:: call :sizecheck "%targetdir%" %targetsize% 12

:: Check if installed content has correct permissions
:: Error Code 13 if permissions are incorrect
:: call :permcheck "%targetdir%" %permissions% 13

:: All checks passed, create event stating that software installed correctly
eventcreate /ID 100 /L Application /T INFORMATION /D "%software% installed correctly" /SO "SPS_TECH"
::Exit out before we start executing these scriptlets
GOTO :END


:: Written by KC
:filecheck
	:: %1 file to check
	:: %2 error code
	:: %~1 strips quotes from passed string reference http://ss64.com/nt/syntax-args.html
	set chk=%~1
	echo   Checking: %chk%
	if not exist "%chk%"  echo    Doesn't Exist %chk% 
	if not exist "%chk%" eventcreate /ID 100 /L Application /T ERROR /D "Failed to install %software%, file %chk% is missing" /SO "SPS_TECH"
	if not exist "%chk%"  exit %2
	set chk=
	GOTO :eof


:: Written by HG
:permcheck
	:: %1 Directory to check
	set dir=%~1
	:: %2 Security Group + Permissions to check
	:: Example - Local user has modify "BUILTIN\Users:(OI)(CI)(M)"
	set perm=%~2
	:: Error Code is %3
	icacls "%dir%" | Find "%perm%"
	set res=%ERRORLEVEL%
	if %res% NEQ 0 echo "%1 Permissions incorrect"
	if %res% NEQ 0 eventcreate /ID 100 /L Application /T ERROR /D "Failed to install %software%, permissions on %dir% are incorrect" /SO "SPS_TECH"
	if %res% NEQ 0 exit %3
	echo "%dir% Permissions Correct"
	set dir=
	set perm=
	set res=
	GOTO :eof
	
:: Written by KC modified by HG
:sizecheck
	::%1 Directory to check
	set dir=%~1
	::%2 Size the directory should be
	set size=%~2
	::%3 is the exit code
	for /F "tokens=3*" %%i in ('dir /-C "%dir%" ^|Find "bytes" ^|find /v "free"') Do set actualsize= %%i
	echo     Content folder size is %actualsize% and should be %size%
	if %actualsize% NEQ %size% eventcreate /ID 100 /L Application /T ERROR /D "Failed to install %software%, installation directory failed size check" /SO "SPS_TECH"
	if %actualsize% NEQ %size% exit %3
	echo   OK:    Content Folder size	
	set dir=
	set perm=
	set actualsize=
	GOTO :eof